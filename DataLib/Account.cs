//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class Account
    {
        public Account()
        {
            this.TheoryCourses = new HashSet<Course>();
            this.CourseRegistrations = new HashSet<CourseRegistration>();
            this.PracticeCourses = new HashSet<Course>();
            this.Memberships = new HashSet<Membership>();
        }
    
        public int ID { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string EncryptCode { get; set; }
        public int FalcultyID { get; set; }
    
        public virtual Falculty Falculty { get; set; }
        public virtual AccountInfo AccountInfo { get; set; }
        public virtual ICollection<Course> TheoryCourses { get; set; }
        public virtual ICollection<CourseRegistration> CourseRegistrations { get; set; }
        public virtual ICollection<Course> PracticeCourses { get; set; }
        public virtual ICollection<Membership> Memberships { get; set; }
    }
}
